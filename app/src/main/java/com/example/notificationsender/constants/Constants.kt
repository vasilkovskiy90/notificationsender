package com.example.notificationsender.constants

object Constants {

    const val MVPMODEL_SQL_QUERY_STRING = "Select * from "
    const val CHANNEL_ID = "SenderService Notification"
    const val ID = 1
    const val REMOTE_INPUT_RESULT_KEY = "KEY_TEXT"
    const val PENDING_INTENT_REQUEST_CODE = 101
    const val INTENT_NEW_MESSAGE_KEY = "NEW MESSAGE KEY"
    const val NOTIFICATION_NAME = "Personal Notification"
    const val NOTIFICATION_CHANNEL_DESCRIPTION = "Include all the personal notifications"
    const val NOTIFICATION_TIMER_NAME = "notificationTimer"


    const val DATABASE_NAME = "SenderDB"
    const val DATABASE_TABLE_NAME = "data"
    const val DATABASE_COL_MESSAGE = "message"
    const val DATABASE_COL_INTERVAL = "interval"
    const val DATABASE_COL_ID = "id"

    const val MILLIS = 1000
    const val VIBRATION_TIME = 2000
}