package com.example.notificationsender.database


import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.notificationsender.constants.Constants

class DataBaseHandler(var context: Context?) : SQLiteOpenHelper(
    context,

    Constants.DATABASE_NAME, null, 1
) {

    override fun onCreate(db: SQLiteDatabase?) {

        val createTable = "CREATE TABLE " + Constants.DATABASE_TABLE_NAME + " (" +
                Constants.DATABASE_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                Constants.DATABASE_COL_MESSAGE + " VARCHAR(256)," +
                Constants.DATABASE_COL_INTERVAL + " INTEGER)"

        db?.execSQL(createTable)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }

}