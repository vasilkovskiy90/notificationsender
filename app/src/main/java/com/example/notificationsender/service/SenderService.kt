package com.example.notificationsender.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.app.RemoteInput
import com.example.notificationsender.MVP.Model.MVPModel
import com.example.notificationsender.R
import com.example.notificationsender.broadcastreceiver.OnUpdateMessageReceiver
import com.example.notificationsender.constants.Constants
import com.example.notificationsender.database.DataBaseHandler
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate


class SenderService : Service() {

    private val timer = Timer(Constants.NOTIFICATION_TIMER_NAME, true)
    private lateinit var dataBase: MVPModel
    private var action: NotificationCompat.Action? = null
    private lateinit var localReceiver: OnUpdateMessageReceiver


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        dataBaseInit(intent)
        createNotificationChannel()
        localReceiverInit()

        startForeground(Constants.ID, createNotification())
        action = createAction()
        notificationTimer()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        timer.cancel()
        unregisterReceiver(localReceiver)
        super.onDestroy()
    }


    // Create reply action and add the remote input
    private fun createAction(): NotificationCompat.Action? {
        return NotificationCompat.Action.Builder(
            R.drawable.ic_launcher_background,
            applicationContext.getText(R.string.action_button_tittle),
            createPendingIntent()
        )
            .addRemoteInput(createRemoteInput())
            .build()
    }

    // Create a pending intent for the reply button
    private fun createPendingIntent(): PendingIntent? {
        return PendingIntent.getBroadcast(
            this,
            Constants.PENDING_INTENT_REQUEST_CODE,
            createActionIntent(),
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun createActionIntent(): Intent {
        return Intent(this, OnUpdateMessageReceiver::class.java)
    }


    private fun notificationTimer() {
        val interval = dataBase.readData()[0].interval.toString().toLong() * Constants.MILLIS
        timer.scheduleAtFixedRate(
            interval,
            interval
        ) {
            createNotification()
        }
    }


    private fun createRemoteInput(): RemoteInput {
        val remoteInput = RemoteInput.Builder(Constants.REMOTE_INPUT_RESULT_KEY)

        remoteInput.setLabel(applicationContext.getText(R.string.remote_input_text_label))
        return remoteInput.build()
    }

    private fun localReceiverInit() {
        localReceiver = OnUpdateMessageReceiver()
        registerReceiver(localReceiver, IntentFilter())
    }

    private fun dataBaseInit(intent: Intent?) {
        val dataBaseHandler = DataBaseHandler(this)
        dataBase =
            MVPModel(dataBaseHandler)
        if (intent?.getStringExtra(Constants.INTENT_NEW_MESSAGE_KEY) != null) {
            updateDB(intent)
        }
    }

    private fun updateDB(intent: Intent) {
        val message = intent.getStringExtra(Constants.INTENT_NEW_MESSAGE_KEY) as String
        dataBase.updateData(message)
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(
                Constants.CHANNEL_ID,
                Constants.NOTIFICATION_NAME,
                importance
            ).apply {
                description = Constants.NOTIFICATION_CHANNEL_DESCRIPTION
                enableVibration(true)
            }

            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun createNotification(): Notification? {

        val notification = NotificationCompat.Builder(applicationContext, Constants.CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setChannelId(Constants.CHANNEL_ID)
            .setContentTitle(applicationContext.getText(R.string.notification_tittle))
            .setContentText(dataBase.readData().first().textFromUser)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setOngoing(true)
            .setVibrate(LongArray(Constants.VIBRATION_TIME))

        if (action != null) {
            notification.addAction(action)
            createNotificationManager(notification.build())
        }
        return notification.build()
    }

    private fun createNotificationManager(notification: Notification?) {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(Constants.ID, notification)
    }
}

