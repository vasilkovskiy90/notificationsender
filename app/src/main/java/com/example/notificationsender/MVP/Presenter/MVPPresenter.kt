package com.example.notificationsender.MVP.Presenter


import android.content.Intent
import android.os.Build
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.widget.doAfterTextChanged
import com.example.notificationsender.MVP.Model.MVPModel
import com.example.notificationsender.MVP.View.MVPViewMain
import com.example.notificationsender.R
import com.example.notificationsender.common.Utils

import com.example.notificationsender.database.DataBaseHandler
import com.example.notificationsender.service.SenderService
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_main.*


class MVPPresenter : BasePresenter {

    private lateinit var mvpView: MVPViewMain

    private var mvpModel: MVPModel? = null

    /**
     * initialisation of view
     */
    fun attachView(mainActivity: MVPViewMain) {
        mvpView = mainActivity
    }

    /**
     * starting the service
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun startService() {
        if (!isValidateFields()) {
            return
        } else {
            if (!Utils.isServiceRunning(mvpView.applicationContext)) {
                dataBaseInit()
                fillDataBase()
                createIntent()
            } else {
                Toast.makeText(
                    mvpView.applicationContext,
                    mvpView.applicationContext.getText(R.string.service_error_message),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun onValidateEvent(textInputLayout: TextInputLayout):Boolean {
        mvpView.setErrorMessage(textInputLayout)
        textInputLayout.editText?.doAfterTextChanged {
            mvpView.deleteErrorMessage(textInputLayout)
        }
        return false
    }

    /**
     * validating fields from view
     */
    private fun isValidateFields(): Boolean {
        return when {
            mvpView.getStringFromMessageField().isEmpty() -> onValidateEvent(mvpView.textLayoutMessage)
            mvpView.getStringFromIntervalField().isEmpty() -> onValidateEvent(mvpView.textLayoutInterval)
            else -> true
        }
    }

    /**
     * database initialisation
     */
    private fun dataBaseInit() {
        val dataBase = DataBaseHandler(mvpView.applicationContext)
        mvpModel = MVPModel(dataBase)
    }

    /**
     * updating database
     */
    private fun fillDataBase() {
        mvpModel?.deleteData()
        mvpModel?.insertData(mvpView.getDataFromView())
    }

    /**
     * new intent for start service
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createIntent() {
        val intent = Intent(mvpView.applicationContext, SenderService::class.java)
        mvpView.applicationContext.startForegroundService(intent)
    }


    override var isAttached: Boolean
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
        set(value) {}

    override fun attach(view: View) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun detach() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}