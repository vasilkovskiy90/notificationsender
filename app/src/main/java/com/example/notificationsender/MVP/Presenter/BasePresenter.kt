package com.example.notificationsender.MVP.Presenter

import android.view.View

interface BasePresenter {
    var isAttached: Boolean
    fun attach(view: View)
    fun detach()
}