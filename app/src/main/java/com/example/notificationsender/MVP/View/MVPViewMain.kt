package com.example.notificationsender.MVP.View

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import com.example.notificationsender.MVP.Presenter.MVPPresenter
import com.example.notificationsender.R
import com.example.notificationsender.common.DataFromUser
import com.example.notificationsender.service.SenderService
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_main.*


class MVPViewMain : BaseCompatActivity() {

    private lateinit var mvpPresenter: MVPPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initPresenter()

    }

    override fun init(savedInstanceState: Bundle?) {
    }

    /**
     * presenter initiation
     */
    private fun initPresenter() {
        mvpPresenter = MVPPresenter()
        mvpPresenter.attachView(this)
    }

    /**
     * onClickListener for Start button
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun onClickStart(view: View) {
        mvpPresenter.startService()
    }

    /**
     * onClickListener for Stop button
     */
    fun onClickStop(view: View) {
        stopService()
    }

    /**
     * on stop button action
     */
    private fun stopService() {
        val intent = Intent(applicationContext, SenderService::class.java)
        this.stopService(intent)
    }

    /**
     * error message from presenter
     */
    fun setErrorMessage(textLayout: TextInputLayout?) {
        textLayout?.error = applicationContext.getText(R.string.input_text_layout_error_message)
    }

    fun deleteErrorMessage(textLayout: TextInputLayout?){
        textLayout?.error=null
    }

    /**
     * returns data Class with strings filled by user
     */
    fun getDataFromView(): DataFromUser {
        return DataFromUser(
            textFromUser.text.toString(),
            interval.text.toString().toLong()
        )
    }

    /**
     * getter for message string
     */
    fun getStringFromMessageField(): String {
        return textFromUser.text.toString()
    }

    /**
     * getter for interval string
     */
    fun getStringFromIntervalField(): String {
        return interval.text.toString()
    }


}

