package com.example.notificationsender.MVP.Model

import android.content.ContentValues
import com.example.notificationsender.common.DataFromUser
import com.example.notificationsender.constants.Constants
import com.example.notificationsender.database.DataBaseHandler

class MVPModel(var dataBaseHandler: DataBaseHandler) {

    /**
     * inserting data in database
     */
    fun insertData(data: DataFromUser) {
        val db = dataBaseHandler.writableDatabase
        val cv = ContentValues()
        cv.put(Constants.DATABASE_COL_MESSAGE, data.textFromUser)
        cv.put(Constants.DATABASE_COL_INTERVAL, data.interval)

        db?.insert(Constants.DATABASE_TABLE_NAME, null, cv)
        db.close()
    }

    /**
     * reading data from database
     */
    fun readData(): MutableList<DataFromUser> {
        val list: MutableList<DataFromUser> = arrayListOf()
        val db = dataBaseHandler.readableDatabase
        val result =
            db?.rawQuery(Constants.MVPMODEL_SQL_QUERY_STRING + Constants.DATABASE_TABLE_NAME, null)
        if (result != null) {
            if (result.moveToFirst()) {
                do {
                    val data = DataFromUser()
                    data.id =
                        result.getString(result.getColumnIndex(Constants.DATABASE_COL_ID)).toInt()
                    data.textFromUser =
                        result.getString(result.getColumnIndex(Constants.DATABASE_COL_MESSAGE))
                    data.interval =
                        result.getString(result.getColumnIndex(Constants.DATABASE_COL_INTERVAL))
                            .toLong()
                    list.add(data)
                } while (result.moveToNext())
            }
        }
        result?.close()
        db?.close()
        return list
    }

    /**
     * deleting data from database
     */
    fun deleteData() {
        val db = dataBaseHandler.writableDatabase
        db?.delete(Constants.DATABASE_TABLE_NAME, null, null)
        db?.close()
    }


    fun updateData(newMessage: String) {
        val oldMessage = readData().first().textFromUser
        val db = dataBaseHandler.writableDatabase
        val cv = ContentValues()
        cv.put(Constants.DATABASE_COL_MESSAGE, newMessage)
        db.update(
            Constants.DATABASE_TABLE_NAME, cv, "message=?",
            arrayOf(oldMessage)
        )

        db.close()
    }


}