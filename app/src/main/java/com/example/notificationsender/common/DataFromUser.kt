package com.example.notificationsender.common

class DataFromUser {
    var textFromUser: String = ""
    var interval: Long = 0
    var id: Int = 0

    constructor(textFromUser: String, interval: Long) {
        this.textFromUser = textFromUser
        this.interval = interval
    }
    constructor()
}