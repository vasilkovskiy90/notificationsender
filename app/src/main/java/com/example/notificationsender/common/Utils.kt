package com.example.notificationsender.common

import android.app.ActivityManager
import android.content.Context
import com.example.notificationsender.service.SenderService

object Utils {
    fun isServiceRunning(context: Context): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for(service: ActivityManager.RunningServiceInfo in manager.getRunningServices(Int.MAX_VALUE)){
            if(SenderService::class.java.name == service.service.className)
                return true
        }
        return false
    }
}