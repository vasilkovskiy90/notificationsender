package com.example.notificationsender.broadcastreceiver


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import com.example.notificationsender.service.SenderService


class OnRebootReceiver : BroadcastReceiver() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context, intent: Intent) {
        //condition for reboot action
        if (Intent.ACTION_BOOT_COMPLETED == intent.action) {
            val rebootIntent = Intent(context, SenderService::class.java)
            context.startForegroundService(rebootIntent)
        }
    }
}
