package com.example.notificationsender.broadcastreceiver

import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.RemoteInput
import com.example.notificationsender.common.Utils
import com.example.notificationsender.constants.Constants
import com.example.notificationsender.service.SenderService



class OnUpdateMessageReceiver : BroadcastReceiver() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context, intent: Intent) {
            if(Utils.isServiceRunning(context)){

                val newMessageIntent= Intent(context, SenderService::class.java)
                context.stopService(newMessageIntent)
                newMessageIntent.putExtra(
                    Constants.INTENT_NEW_MESSAGE_KEY,
                    newActionMessage(intent).toString()
                )
                context.startService(newMessageIntent)
            }
    }

    private fun newActionMessage(paramIntent: Intent): CharSequence? {
        val remoteInput = RemoteInput.getResultsFromIntent(paramIntent)
        return remoteInput?.getCharSequence(Constants.REMOTE_INPUT_RESULT_KEY)
    }
}
